import React from 'react';
import Hello from './Hello.jsx';

const App = () => (
  <React.Fragment>
    <Hello />
  </React.Fragment>
);

export default App;
