import React, { Component } from "react";

export default class Hello extends Component {
  state = {
    tasks: [],
    newTaskToAdd: ""
  };

  render() {
    return (
      <div className="container my-5">
        <div className="row">
          <div className="col-sm-12">
            <div className="card my-2">
              <div className="card-body">
                <h5 className="card-title">Task List</h5>
                <input
                  className="form-control form-control-lg my-2"
                  type="text"
                  placeholder="New Task"
                  value={this.state.newTaskToAdd}
                  onChange={e =>
                    this.setState({
                      newTaskToAdd: e.target.value
                    })
                  }
                />
                <button
                  type="button"
                  className="btn btn-primary  my-2"
                  onClick={() =>
                    this.setState({
                      tasks: [...this.state.tasks, this.state.newTaskToAdd],
                      newTaskToAdd: ""
                    })
                  }
                >
                  ADD TASK
                </button>
                <h5 className="card-title">Tasks</h5>
                <ul className="list-group">
                  {this.state.tasks.map((item, idx) => {
                    return (
                      <li className="list-group-item">
                        {item}
                        <a
                          href="#"
                          onClick={() => {
                            this.setState({
                              tasks: this.state.tasks.filter(
                                (itm, i) => i !== idx
                              )
                            });
                          }}
                        >
                          <span className="badge float-right">
                            <i className="fas fa-times fa-lg" />
                          </span>
                        </a>
                      </li>
                    );
                  })}
                </ul>
                <input
                  className="form-control form-control-lg my-2"
                  id="filter"
                  type="text"
                  disabled
                  placeholder="Filter Tasks"
                />
                <button
                  type="button"
                  className="btn btn-danger btn-md my-2"
                  onClick={() => this.setState({ tasks: [] })}
                >
                  CLEAR TASKS
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
